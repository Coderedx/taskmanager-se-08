FROM java:8
COPY ./target/task-manager-1.0.0.jar /opt/task-manager-1.0.0.jar
WORKDIR /opt

ENTRYPOINT ["java", "-jar", "task-manager-1.0.0.jar.jar"]