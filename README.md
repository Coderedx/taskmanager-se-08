#### Требования к Software
- JRE 1.8
#### Стек Технологий
- Java SE 1.8
- Maven
#### Контактные данные разработчика
- shumovsv@gmail.com
#### Команды для сборки
```
mvn clean install
```
#### Команды для запуска приложения
```
java -jar ./<.jar file>
```