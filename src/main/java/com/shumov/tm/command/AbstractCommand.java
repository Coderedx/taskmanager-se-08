package com.shumov.tm.command;

import com.shumov.tm.api.service.ServiceLocator;

import com.shumov.tm.util.constant.UserRoleType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;
    @NotNull
    protected List<UserRoleType> roleTypes = new ArrayList<>();
    @NotNull
    public abstract String command();
    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
    @NotNull
    public List<UserRoleType> getRoleTypes() {
        return roleTypes;
    }

    public abstract void initRoles();
}
