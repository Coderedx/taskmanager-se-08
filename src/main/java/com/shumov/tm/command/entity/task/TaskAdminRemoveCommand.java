package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class TaskAdminRemoveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-remove-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove any task by id";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = serviceLocator.getTerminalService().nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isCorrectData(taskId);
        taskService.removeTaskById(taskId);
        System.out.println("TASK HAS BEEN REMOVED SUCCESSFULLY");
    }
}
