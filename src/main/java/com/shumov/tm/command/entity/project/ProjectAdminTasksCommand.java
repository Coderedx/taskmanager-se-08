package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class ProjectAdminTasksCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-tasks-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Review all tasks";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[REVIEW PROJECT TASKS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = serviceLocator.getTerminalService().nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isCorrectData(id);
        projectService.getProject(id);
        System.out.println("[TASK LIST]");
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        for (@NotNull final Task task : taskService.getProjectTasks(id)) {
            System.out.println("TASK ID: " + task.getId() + "\nTASK NAME: " + task.getName()+
                    "\nUSER ID: "+ task.getOwnerId()+"\n");
        }
    }
}
