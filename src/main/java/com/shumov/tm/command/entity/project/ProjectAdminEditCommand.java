package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class ProjectAdminEditCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-edit-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change name of any project";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[EDIT PROJECT NAME BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = serviceLocator.getTerminalService().nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isCorrectData(id);
        System.out.println("ENTER NEW PROJECT NAME:");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        projectService.isCorrectData(name);
        projectService.editProjectNameById(id,name);
        System.out.println("PROJECT NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}
