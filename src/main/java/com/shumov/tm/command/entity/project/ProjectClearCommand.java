package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-clear";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[CLEAR PROJECTS]");
        System.out.println("[Enter \"Y\" if you confirm deletion of all projects]".toUpperCase());
        @NotNull final String confirm = serviceLocator.getTerminalService().nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        @NotNull final User user = serviceLocator.getCurrentUser();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        projectService.clearData(user.getId(), taskService);
        System.out.println("[ALL PROJECTS HAVE BEEN DELETED SUCCESSFULLY]");
    }

    public void initRoles(){
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}

