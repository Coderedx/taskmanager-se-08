package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class ProjectEditCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit project name";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[EDIT PROJECT NAME BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = serviceLocator.getTerminalService().nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isCorrectData(id);
        System.out.println("ENTER NEW PROJECT NAME:");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        projectService.isCorrectData(name);
        @NotNull final User user = serviceLocator.getCurrentUser();
        projectService.editProjectNameById(user.getId(),id,name);
        System.out.println("PROJECT NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}
