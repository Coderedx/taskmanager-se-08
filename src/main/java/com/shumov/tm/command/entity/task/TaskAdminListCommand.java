package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class TaskAdminListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-list-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all tasks of all users";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK LIST]");
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        for (@NotNull final Task task : taskService.getTaskList()) {
            System.out.println("TASK ID: " + task.getId() + "\nTASK NAME: " + task.getName()
                    + "\nPROJECT ID: "+ task.getIdProject()
                    + "\nUSER ID: "+ task.getOwnerId()+"\n");
        }
    }
}
