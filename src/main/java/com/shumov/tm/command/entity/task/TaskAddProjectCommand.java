package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class TaskAddProjectCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-add";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Add task in project";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[ADD TASK IN PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = serviceLocator.getTerminalService().nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isCorrectData(projectId);
        @NotNull final User user = serviceLocator.getCurrentUser();
        projectService.getProject(user.getId(), projectId);
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = serviceLocator.getTerminalService().nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isCorrectData(taskId);
        @NotNull final Task task = taskService.getTask(user.getId(), taskId);
        task.setIdProject(projectId);
        taskService.addTaskInProject(user.getId(),taskId,task);
        System.out.println("TASK HAS BEEN ADD SUCCESSFULLY");
    }
}
