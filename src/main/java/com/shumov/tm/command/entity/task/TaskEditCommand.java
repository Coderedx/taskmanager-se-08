package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;

import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class TaskEditCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit task name";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[EDIT TASK NAME BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = serviceLocator.getTerminalService().nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isCorrectData(taskId);
        System.out.println("ENTER NEW TASK NAME:");
        @NotNull final String taskName = serviceLocator.getTerminalService().nextLine();
        taskService.isCorrectData(taskName);
        @NotNull final User user = serviceLocator.getCurrentUser();
        taskService.editTaskNameById(user.getId(),taskId,taskName);
        System.out.println("TASK NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}

