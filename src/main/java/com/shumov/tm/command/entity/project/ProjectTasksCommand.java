package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class ProjectTasksCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-tasks";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Review project tasks";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[REVIEW PROJECT TASKS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = serviceLocator.getTerminalService().nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isCorrectData(id);
        @NotNull final User user = serviceLocator.getCurrentUser();
        projectService.getProject(user.getId(), id);
        System.out.println("[TASK LIST]");
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        for (@NotNull final Task task : taskService.getProjectTasks(user.getId(), id)) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName());
        }
    }
}
