package com.shumov.tm.command.entity.user;


import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.HashMd5;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class UserLoginCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "login";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "User login";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.isCorrectLogin(login);
        System.out.println("ENTER PASSWORD:");
        @NotNull final String pass = serviceLocator.getTerminalService().nextLine();
        userService.isCorrectPass(pass);
        @NotNull final User user = userService.getUser(login, HashMd5.getMd5(pass));
        System.out.println("authorization successful".toUpperCase());
        serviceLocator.setCurrentUser(user);
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.GUEST);
    }
}
