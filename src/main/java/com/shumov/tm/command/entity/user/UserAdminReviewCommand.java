package com.shumov.tm.command.entity.user;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class UserAdminReviewCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "admin-review";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "All user review";
    }

    @Override
    public void execute() throws Exception {
        reviewAdmin();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void reviewAdmin() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[USER REVIEW]");
        @NotNull final User user = serviceLocator.getCurrentUser();
        System.out.println("Login: "+ user.getLogin());
        System.out.println("ID: "+ user.getId());
        System.out.println("Role: "+ user.getUserRoleType().toString());
        System.out.println("Description: "+ user.getDescription() +"\n");
    }
}
