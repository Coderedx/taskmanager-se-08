package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class UserRegCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user-reg";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "New user registration";
    }

    @Override
    public void execute() throws Exception {
        createUser();
    }

    private void createUser() throws Exception  {
        if(serviceLocator == null) return;
        System.out.println("[NEW USER REGISTRATION]");
        @NotNull final IUserService userService = serviceLocator.getUserService();
        System.out.println("[ENTER LOGIN FOR NEW USER]");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        userService.isCorrectLogin(login);
        System.out.println("[ENTER PASSWORD FOR NEW USER]");
        @NotNull final String pass = serviceLocator.getTerminalService().nextLine();
        userService.isCorrectPass(pass);
        userService.createNewUser(login, pass);
        System.out.println("NEW USER CREATED SUCCESSFULLY");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
        roleTypes.add(UserRoleType.GUEST);
    }
}
