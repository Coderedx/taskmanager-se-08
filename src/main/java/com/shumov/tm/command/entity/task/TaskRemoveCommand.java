package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class TaskRemoveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-remove";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove selected task";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = serviceLocator.getTerminalService().nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isCorrectData(taskId);
        @NotNull final User user = serviceLocator.getCurrentUser();
        taskService.removeTaskById(user.getId(), taskId);
        System.out.println("TASK HAS BEEN REMOVED SUCCESSFULLY");
    }
}
