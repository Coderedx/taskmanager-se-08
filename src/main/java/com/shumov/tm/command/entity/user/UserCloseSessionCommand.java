package com.shumov.tm.command.entity.user;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class UserCloseSessionCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user-close";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "End current session";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[END CURRENT SESSION]");
        System.out.println("[Enter \"Y\" if you confirm]".toUpperCase());
        @NotNull final String confirm = serviceLocator.getTerminalService().nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        serviceLocator.setCurrentUser(serviceLocator.getGuestUser());
        System.out.println("Current session completed successfully".toUpperCase());
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}
