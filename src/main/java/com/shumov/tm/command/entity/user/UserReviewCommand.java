package com.shumov.tm.command.entity.user;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class UserReviewCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user-review";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "User review";
    }

    @Override
    public void execute() throws Exception {
        reviewUser();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
        roleTypes.add(UserRoleType.USER);
    }

    private void reviewUser() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[USER REVIEW]");
        @NotNull final User user = serviceLocator.getCurrentUser();
        System.out.println("Login: "+ user.getLogin());
        System.out.println("Description: "+ user.getDescription() +"\n");
    }
}
