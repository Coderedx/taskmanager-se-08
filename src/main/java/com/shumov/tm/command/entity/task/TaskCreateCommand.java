package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-create";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK CREATE]\nENTER TASK NAME:");
        @NotNull final String taskName = serviceLocator.getTerminalService().nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isCorrectData(taskName);
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = serviceLocator.getTerminalService().nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isCorrectData(id);
        @NotNull final Project project = projectService.getProject(id);
        @NotNull final User user = serviceLocator.getCurrentUser();
        taskService.createTask(user.getId(), taskName, project.getId());
        System.out.println("TASK HAS BEEN CREATED SUCCESSFULLY");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}
