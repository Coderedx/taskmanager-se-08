package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class ProjectAdminRemoveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-remove-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove any project by id";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = serviceLocator.getTerminalService().nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        projectService.isCorrectData(id);
        projectService.removeProjectById(id, taskService);
        System.out.println("PROJECT HAS BEEN REMOVED SUCCESSFULLY");
    }
}
