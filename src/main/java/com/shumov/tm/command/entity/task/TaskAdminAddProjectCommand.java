package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class TaskAdminAddProjectCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-add-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Add task in any project";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[ADD TASK IN PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = serviceLocator.getTerminalService().nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isCorrectData(projectId);
        projectService.getProject(projectId);
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = serviceLocator.getTerminalService().nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isCorrectData(taskId);
        @NotNull final Task task = taskService.getTask(taskId);
        task.setIdProject(projectId);
        taskService.addTaskInProject(taskId,task);
        System.out.println("TASK HAS BEEN ADD SUCCESSFULLY");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }
}
