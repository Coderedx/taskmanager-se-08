package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class ProjectListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-list";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[PROJECT LIST]");
        @NotNull final User user = serviceLocator.getCurrentUser();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        for (@NotNull final Project project : projectService.getProjectList(user.getId())) {
            System.out.println("PROJECT ID: " + project.getId() + " PROJECT NAME: " + project.getName());
        }
    }
}
