package com.shumov.tm.command.help;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class ExitCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "exit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Exit";
    }

    @Override
    public void execute() throws Exception {
        System.exit(0);
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.GUEST);
    }

}
