package com.shumov.tm.entity;



import com.shumov.tm.api.entity.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@Setter
@Getter
@NoArgsConstructor
public class Project extends AbstractEntity implements Entity {
    @NotNull
    private String name = "";
    @NotNull
    private String description = "";
    @Nullable
    private Date dateStart;
    @Nullable
    private Date dateFinish;

    public Project(@NotNull final String name,
                   @NotNull final String ownerId){
        this.name = name;
        this.ownerId = ownerId;
    }
}
