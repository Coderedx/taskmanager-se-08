package com.shumov.tm.entity;

import com.shumov.tm.api.entity.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@Setter
@Getter
@NoArgsConstructor
public class Task extends AbstractEntity implements Entity {

    @NotNull
    private String name = "";
    @NotNull
    private String description = "";
    @NotNull
    private String idProject = "";
    @Nullable
    private Date dateStart;
    @Nullable
    private Date dateFinish;


    public Task(@NotNull final String ownerId,
                @NotNull final String name,
                @NotNull final String idProject){
        this.name = name;
        this.idProject = idProject;
        this.ownerId = ownerId;
    }
}
