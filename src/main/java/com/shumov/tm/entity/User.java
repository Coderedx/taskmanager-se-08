package com.shumov.tm.entity;

import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.util.HashMd5;
import com.shumov.tm.util.constant.UserRoleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@NoArgsConstructor
public class User extends AbstractEntity implements Entity {

    @NotNull
    private String login = "guest";
    @NotNull
    private String passwordHash = "guest";
    @NotNull
    private UserRoleType userRoleType = UserRoleType.GUEST;
    @Nullable
    private String description = "guest";

    public User(@NotNull final String login,
                @NotNull final String password){
        this.login = login;
        this.passwordHash = HashMd5.getMd5(password);
        this.userRoleType = UserRoleType.USER;
    }

    public User(@NotNull final String login,
                @NotNull final String password,
                @NotNull UserRoleType userRoleType) {
        this.login = login;
        this.passwordHash = HashMd5.getMd5(password);
        this.userRoleType = userRoleType;
    }
}
