package com.shumov.tm.api.repository;

import com.shumov.tm.entity.User;
import org.jetbrains.annotations.NotNull;

public interface IUserRepository extends IRepository<User> {

    @Override
    @NotNull
    User findOne(@NotNull final String password,
                 @NotNull final String login) throws Exception;

    @Override
    void persist(@NotNull final User user) throws Exception;
}
