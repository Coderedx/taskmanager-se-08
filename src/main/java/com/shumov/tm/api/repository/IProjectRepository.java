package com.shumov.tm.api.repository;

import com.shumov.tm.entity.Project;

public interface IProjectRepository extends IRepository<Project> {

}
