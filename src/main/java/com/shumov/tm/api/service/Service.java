package com.shumov.tm.api.service;

import com.shumov.tm.api.entity.Entity;
import org.jetbrains.annotations.Nullable;

public interface Service {

    void isCorrectData(@Nullable final String data) throws Exception;

    void isCorrectObject(@Nullable final Entity entity) throws Exception;
}
