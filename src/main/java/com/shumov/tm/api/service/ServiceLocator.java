package com.shumov.tm.api.service;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ServiceLocator {

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITerminalService getTerminalService();

    @NotNull
    List<AbstractCommand> getCommands();

    @NotNull
    User getCurrentUser();

    @NotNull
    User getGuestUser();

    @NotNull
    IUserService getUserService();

    void setCurrentUser(@NotNull final User currentUser);

}
