package com.shumov.tm.api.service;

import com.shumov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskService extends Service {

    void createTask(@Nullable final String ownerId,
                    @Nullable final String name,
                    @Nullable final String projectId) throws Exception;

    @NotNull
    List<Task> getTaskList() throws Exception;

    @NotNull
    List<Task> getTaskList(@Nullable final String ownerId) throws Exception;

    @NotNull
    Task getTask(@Nullable final String id) throws Exception;

    @NotNull
    Task getTask(@Nullable final String ownerId,
                 @Nullable final String id) throws Exception;

    @NotNull
    List<Task> getProjectTasks(@Nullable final String projectId) throws Exception;

    @NotNull
    List<Task> getProjectTasks(@Nullable final String ownerId,
                               @Nullable final String projectId) throws Exception;

    void clearData() throws Exception;

    void clearData(@Nullable final String ownerId) throws Exception;

    void editTaskNameById(@Nullable final String id,
                          @Nullable final String name) throws Exception;

    void editTaskNameById(@Nullable final String ownerId,
                          @Nullable final String id,
                          @Nullable final String name) throws Exception;

    void removeTaskById(@Nullable final String id) throws Exception;

    void removeTaskById(@Nullable final String ownerId,
                        @Nullable final String id) throws Exception;

    void addTaskInProject(@Nullable final String id,
                          @Nullable final Task task) throws Exception;

    void addTaskInProject(@Nullable final String ownerId,
                          @Nullable final String id,
                          @Nullable final Task task) throws Exception;
}
