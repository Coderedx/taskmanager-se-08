package com.shumov.tm.service;

import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.entity.Task;
import com.shumov.tm.exception.entity.EntityListIsEmptyException;
import com.shumov.tm.repository.TaskRepository;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class TaskService extends AbstractService implements ITaskService {

    @NotNull
    protected IRepository<Task> repository = new TaskRepository();

    public TaskService(@NotNull final IRepository<Task> repository) {
        this.repository = repository;
    }

    @Override
    public void createTask(@Nullable final String ownerId,
                           @Nullable final String name,
                           @Nullable final String projectId) throws Exception {
        isCorrectData(ownerId);
        isCorrectData(name);
        isCorrectData(projectId);
        @NotNull final Task task = new Task(ownerId, name, projectId);
        repository.persist(task);
    }

    @Override
    @NotNull
    public List<Task> getTaskList() throws Exception {
        return repository.findAll();
    }

    @Override
    @NotNull
    public List<Task> getTaskList(@Nullable final String ownerId) throws Exception {
        isCorrectData(ownerId);
        return repository.findAll(ownerId);
    }

    @Override
    @NotNull
    public Task getTask(@Nullable final String id) throws Exception {
        isCorrectData(id);
        return repository.findOne(id);
    }

    @Override
    @NotNull
    public Task getTask(@Nullable final String ownerId,
                        @Nullable final String id) throws Exception {
        isCorrectData(ownerId);
        isCorrectData(id);
        return repository.findOne(ownerId, id);
    }

    @Override
    @NotNull
    public List<Task> getProjectTasks(@Nullable final String projectId) throws Exception {
        isCorrectData(projectId);
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull final Task task : repository.findAll()) {
            @NotNull final String taskProjectId = task.getIdProject();
            if(projectId.equals(taskProjectId)){
                tasks.add(task);
            }
        }
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> getProjectTasks(@Nullable final String ownerId,
                                      @Nullable final String projectId) throws Exception {
        isCorrectData(ownerId);
        isCorrectData(projectId);
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull final Task task : repository.findAll(ownerId)) {
            @NotNull final String taskProjectId = task.getIdProject();
            if(projectId.equals(taskProjectId)){
                tasks.add(task);
            }
        }
        return tasks;
    }

    @Override
    public void clearData() throws Exception {
        repository.removeAll();
    }

    @Override
    public void clearData(@Nullable final String ownerId) throws Exception{
        isCorrectData(ownerId);
        repository.removeAll(ownerId);
    }

    @Override
    public void editTaskNameById(@Nullable final String id,
                                 @Nullable final String name) throws Exception {
        isCorrectData(id);
        isCorrectData(name);
        @NotNull final Task task = repository.findOne(id);
        task.setName(name);
        repository.merge(id, task);
    }

    @Override
    public void editTaskNameById(@Nullable final String ownerId,
                                 @Nullable final String id,
                                 @Nullable final String name) throws Exception {
        isCorrectData(ownerId);
        isCorrectData(id);
        isCorrectData(name);
        @NotNull final Task task = repository.findOne(ownerId, id);
        task.setName(name);
        repository.merge(id, task);
    }

    @Override
    public void removeTaskById(@Nullable final String id) throws Exception{
        isCorrectData(id);
        repository.remove(id);
    }

    @Override
    public void removeTaskById(@Nullable final String ownerId,
                               @Nullable final String id) throws Exception {
        isCorrectData(ownerId);
        isCorrectData(id);
        repository.remove(ownerId, id);
    }

    @Override
    public void addTaskInProject(@Nullable final String id,
                                 @Nullable final Task task) throws Exception {
        isCorrectData(id);
        isCorrectObject(task);
        repository.merge(id,task);
    }

    @Override
    public void addTaskInProject(@Nullable final String ownerId,
                                 @Nullable final String id,
                                 @Nullable final Task task) throws Exception {
        isCorrectData(ownerId);
        isCorrectData(id);
        isCorrectObject(task);
        repository.findOne(ownerId, id);
        repository.merge(id,task);
    }
}
