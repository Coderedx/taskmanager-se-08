package com.shumov.tm.service;

import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.api.service.Service;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

@NoArgsConstructor
public abstract class AbstractService implements Service {

    @Override
    public final void isCorrectData(@Nullable final String name) throws Exception {
        if (name==null || name.isEmpty()){
            throw new IOException("Incorrect data entered".toUpperCase());
        }
    }

    public final void isCorrectObject(@Nullable final Entity entity) throws Exception {
        if (entity==null){
            throw new IOException("Incorrect object".toUpperCase());
        }
    }
}
