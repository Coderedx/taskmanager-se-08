package com.shumov.tm.service;

import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Task;
import com.shumov.tm.repository.ProjectRepository;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public class ProjectService extends AbstractService implements IProjectService {

    @NotNull
    protected IRepository<Project> repository = new ProjectRepository();

    public ProjectService(@NotNull final IRepository<Project> repository) {
        this.repository = repository;
    }

    @Override
    public void createProject(@Nullable final String ownerId,
                              @Nullable final String name) throws Exception  {
        isCorrectData(ownerId);
        isCorrectData(name);
        @NotNull final Project project = new Project(name, ownerId);
        repository.persist(project);
    }

    @Override
    @NotNull
    public List<Project> getProjectList() throws Exception {
        return repository.findAll();
    }

    @Override
    @NotNull
    public List<Project> getProjectList(@Nullable final String ownerId) throws Exception {
        isCorrectData(ownerId);
        return repository.findAll(ownerId);
    }

    @Override
    @NotNull
    public Project getProject(@Nullable final String id) throws Exception {
        isCorrectData(id);
        return repository.findOne(id);
    }

    @Override
    @NotNull
    public final Project getProject(@Nullable final String ownerId,
                                    @Nullable final String id) throws Exception {
        isCorrectData(ownerId);
        isCorrectData(id);
        return repository.findOne(ownerId, id);
    }

    @Override
    public final void editProjectNameById(@Nullable final String id,
                                          @Nullable final String name) throws Exception {
        isCorrectData(id);
        isCorrectData(name);
        @NotNull final Project project = repository.findOne(id);
        project.setName(name);
        repository.merge(id, project);
    }

    @Override
    public final void editProjectNameById(@Nullable final String ownerId,
                                          @Nullable final String id,
                                          @Nullable final String name) throws Exception {
        isCorrectData(ownerId);
        isCorrectData(id);
        isCorrectData(name);
        @NotNull final Project project = repository.findOne(ownerId, id);
        project.setName(name);
        repository.merge(id, project);
    }

    @Override
    public final void removeProjectById(@Nullable final String id,
                                        @NotNull final ITaskService taskService) throws Exception {
        isCorrectData(id);
        repository.remove(id);
        @NotNull final List<Task> list = taskService.getTaskList();
        for (@NotNull final Task task : list) {
            @NotNull final String taskProjectId = task.getIdProject();
            if(!list.isEmpty() && id.equals(taskProjectId)) {
                @NotNull final String taskId = task.getId();
                taskService.removeTaskById(taskId);
            }
        }
    }

    @Override
    public final void removeProjectById(@Nullable final String ownerId,
                                        @Nullable final String id,
                                        @NotNull final ITaskService taskService) throws Exception {
        isCorrectData(ownerId);
        isCorrectData(id);
        repository.remove(ownerId, id);
        @NotNull final List<Task> list = taskService.getTaskList(ownerId);
        for (@NotNull final Task task : list) {
            @NotNull final String taskProjectId = task.getIdProject();
            if(id.equals(taskProjectId)){
                @NotNull final String taskId = task.getId();
                taskService.removeTaskById(ownerId, taskId);
            }
        }
    }

    @Override
    public final void clearData(@NotNull final ITaskService taskService) throws Exception {
        repository.removeAll();
        taskService.clearData();
    }

    @Override
    public final void clearData(@Nullable final String ownerId,
                                @NotNull final ITaskService taskService) throws Exception {
        isCorrectData(ownerId);
        repository.removeAll(ownerId);
        taskService.clearData(ownerId);
    }
}
