package com.shumov.tm.service;


import com.shumov.tm.api.service.ITerminalService;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

@NoArgsConstructor
public class TerminalService extends AbstractService implements ITerminalService {

    @NotNull
    private Scanner terminalService = new Scanner(System.in);

    @Override
    @NotNull
    public String nextLine(){
        return terminalService.nextLine();
    }
}
