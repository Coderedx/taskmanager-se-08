package com.shumov.tm.service;

import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.entity.User;
import com.shumov.tm.repository.UserRepository;
import com.shumov.tm.util.constant.UserRoleType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

@NoArgsConstructor
public class UserService extends AbstractService implements IUserService {

    @NotNull
    protected IRepository<User> repository = new UserRepository();

    public UserService(@NotNull final IRepository<User> repository) {
        this.repository = repository;
    }

    @Override
    @NotNull
    public final User getUser(@Nullable final String login,
                              @Nullable final String password) throws Exception {
        isCorrectLogin(login);
        isCorrectPass(password);
        @NotNull final User user = repository.findOne(login);
        @NotNull final String passwordHash = user.getPasswordHash();
        if(passwordHash.equals(password)) return user;
        else throw new IOException("Wrong Password");
    }

    @Override
    public final void mergeUser(@Nullable final User user) throws Exception {
        isCorrectObject(user);
        @NotNull final String id = user.getId();
        repository.merge(id, user);
    }

    @Override
    public final void createNewUser(@Nullable final String login,
                                    @Nullable final String password) throws Exception {
        isCorrectLogin(login);
        isCorrectPass(password);
        @NotNull final User user = new User(login, password);
        repository.persist(user);
    }

    @Override
    public final void createNewUser(@Nullable final String login,
                                    @Nullable final String password,
                                    @NotNull final UserRoleType userRole) throws Exception {
        isCorrectLogin(login);
        isCorrectPass(password);
        @NotNull final User user = new User(login, password, userRole);
        repository.persist(user);
    }

    @Override
    public final void isCorrectLogin(@Nullable final String login) throws IOException {
        if (login==null || login.isEmpty()){
            throw new IOException("Incorrect login!".toUpperCase());
        }
    }

    @Override
    public final void isCorrectPass(@Nullable final String pass) throws IOException{
        if(pass==null || pass.isEmpty()){
            throw new IOException("Incorrect password!".toUpperCase());
        }
    }

    @Override
    public final void isCorrectDescription(@Nullable final String description) throws IOException {
        if(description==null || description.isEmpty()){
            throw new IOException("Incorrect description!".toUpperCase());
        }
    }
}
