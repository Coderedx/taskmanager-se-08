package com.shumov.tm.bootstrap;


import com.shumov.tm.api.service.*;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.exception.command.CommandCorruptException;
import com.shumov.tm.exception.command.CommandWrongException;
import com.shumov.tm.repository.ProjectRepository;
import com.shumov.tm.repository.TaskRepository;
import com.shumov.tm.repository.UserRepository;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.service.TaskService;
import com.shumov.tm.service.TerminalService;
import com.shumov.tm.service.UserService;
import com.shumov.tm.util.constant.UserRoleType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.*;

@NoArgsConstructor
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    @NotNull
    private final IProjectService projectService = new ProjectService(new ProjectRepository());
    @NotNull
    private final ITaskService taskService = new TaskService(new TaskRepository());
    @NotNull
    private final IUserService userService = new UserService(new UserRepository());
    @NotNull
    private final ITerminalService terminalService = new TerminalService();
    @NotNull
    private User guestUser = new User("guest","guest", UserRoleType.GUEST);
    @NotNull
    private User currentUser = guestUser;

    public void init(@NotNull Class[] classes) throws Exception {
        commands.clear();
        initUsers();
        initCommands(classes);
        start();
    }

    private void registry(@NotNull final AbstractCommand command) throws CommandCorruptException {
        @NotNull final String cliCommand = command.command();
        @NotNull final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty()) {
            throw new CommandCorruptException();
        }
        if (cliDescription == null || cliDescription.isEmpty()) {
            throw new CommandCorruptException();
        }
        command.setServiceLocator(this);
        commands.put(command.command(), command);
    }

    private void start() {
        System.out.println("[WELCOME TO TASK MANAGER]");
        System.out.println("[ENTER \"help\" TO GET COMMAND LIST]");
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            System.out.println("\nENTER COMMAND:");
            command = terminalService.nextLine();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(@NotNull final String command) throws Exception {
        if (command == null || command.isEmpty()) {
            throw new CommandWrongException();
        }
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            throw new CommandWrongException();
        }
        // Проверка валидности выполнения операции.
        if(isSecureCommand(abstractCommand)){
            abstractCommand.execute();
        } else {
            throw new IOException("access error!".toUpperCase());
        }
    }

    private void initUsers() throws Exception {
        try {
            userService.createNewUser("user", "user", UserRoleType.USER);
            userService.createNewUser("admin", "admin", UserRoleType.ADMIN);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    private void initCommands(@NotNull final Class[] classes) throws Exception {
        for (@NotNull final Class clazz : classes){
            if(AbstractCommand.class.isAssignableFrom(clazz)) {
                @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();
                try {
                    command.initRoles();
                    registry(command);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private boolean isSecureCommand(@NotNull final AbstractCommand command) {
        @NotNull final UserRoleType role = currentUser.getUserRoleType();
        @NotNull final List<UserRoleType> roles = command.getRoleTypes();
        return roles.contains(role);
    }

    @NotNull
    public ITerminalService getTerminalService() {
        return terminalService;
    }
    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }
    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }
    @NotNull
    public IUserService getUserService() {
        return userService;
    }
    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }
    @NotNull
    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(@NotNull final User currentUser) {
        this.currentUser = currentUser;
    }
    @NotNull
    public User getGuestUser() {
        return guestUser;
    }
}
