package com.shumov.tm.repository;

import com.shumov.tm.api.repository.ITaskRepository;
import com.shumov.tm.entity.Task;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

}
