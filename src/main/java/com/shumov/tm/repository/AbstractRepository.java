package com.shumov.tm.repository;

import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.exception.entity.EntityIsAlreadyExistException;
import com.shumov.tm.exception.entity.EntityListIsEmptyException;
import com.shumov.tm.exception.entity.EntityNotExistException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public abstract class AbstractRepository<T extends Entity> implements IRepository<T> {

    @NotNull
    protected final Map<String, T> entityMap = new LinkedHashMap<>();

    @Override
    @NotNull
    public List<T> findAll() throws Exception {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty) return new ArrayList<>();
        else return new ArrayList<>(entityMap.values());
    }

    @Override
    @NotNull
    public List<T> findAll(@NotNull final String ownerId) throws Exception {
        @NotNull final List<T> list = new ArrayList<>();
        for (@NotNull final T entity: findAll()) {
            @NotNull final String entityOwnerId = entity.getOwnerId();
            if(entityOwnerId.equals(ownerId)) list.add(entity);
        }
        final boolean listIsEmpty = list.isEmpty();
        if(listIsEmpty) return new ArrayList<>();
        else return list;
    }

    @Override
    @NotNull
    public T findOne(@NotNull final String entityId) throws Exception {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty) throw new EntityListIsEmptyException();
        final boolean mapContainsKey = entityMap.containsKey(entityId);
        if(mapContainsKey) return entityMap.get(entityId);
        else throw new EntityNotExistException();
    }

    @Override
    @NotNull
    public T findOne(@NotNull final String ownerId,
                     @NotNull final String entityId) throws Exception {
        @NotNull final T entity = findOne(entityId);
        @NotNull final String entityOwnerId = entity.getOwnerId();
        final boolean equality = entityOwnerId.equals(ownerId);
        if (equality) return entity;
        else throw new EntityNotExistException();
    }

    @Override
    public void persist(@NotNull final T entity) throws Exception {
        @NotNull final String entityId = entity.getId();
        final boolean mapContainsKey = entityMap.containsKey(entityId);
        if(mapContainsKey) throw new EntityIsAlreadyExistException();
        else entityMap.put(entity.getId(), entity);
    }

    @Override
    public void merge(@NotNull final String entityId,
                      @NotNull final T entity) {
        entity.setId(entityId);
        entityMap.put(entityId, entity);
    }

    @Override
    public void remove(@NotNull final String entityId) throws Exception {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty)throw new EntityListIsEmptyException();
        final boolean mapContainsKey = entityMap.containsKey(entityId);
        if(!mapContainsKey) throw new EntityNotExistException();
        else entityMap.remove(entityId);
    }

    @Override
    public void remove(@NotNull final String ownerId,
                       @NotNull final String entityId) throws Exception {
        findOne(ownerId, entityId);
        remove(entityId);
    }

    @Override
    public void removeAll() throws Exception {
        final boolean mapIsEmpty = entityMap.isEmpty();
        if(mapIsEmpty) throw new EntityListIsEmptyException();
        else entityMap.clear();
    }

    @Override
    public void removeAll(@NotNull final String ownerId) throws Exception {
        @NotNull final List<T> list = findAll(ownerId);
        for (@NotNull final T entity : list) {
            @NotNull final String entityId = entity.getId();
            remove(entityId);
        }
    }


}
